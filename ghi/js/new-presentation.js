window.addEventListener('DOMContentLoaded', async () => {
    const conferenceRequestUrl = 'http://localhost:8000/api/conferences/';
    const conferenceRequestResponse = await fetch(conferenceRequestUrl);
    if (!conferenceRequestResponse.ok){
        throw "Invalid Request was made fetching Conference list";
    }else{
        const conferenceList = await conferenceRequestResponse.json()
        const conferenceListFormatted = []

        for (let i=0; i< conferenceList['conferences'].length; i++){
            let conferenceListSplit = conferenceList['conferences'][i].href.split('/')
            let conferenceId = conferenceListSplit[conferenceListSplit.length-2]
            conferenceListFormatted.push(`
            <option value="${parseInt(conferenceId)}">${conferenceList['conferences'][i].name}</option>
            `)
        }
        const conferenceListHtml = `
        <option selected value="">Choose a Conference</option>
        ${conferenceListFormatted.join('')}
        `
        const conferenceTag = document.querySelector('.form-select')
        conferenceTag.innerHTML = conferenceListHtml
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event=> {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const jsondata = JSON.parse(json)
        const presentationUrl = `http://localhost:8000/api/conferences/${jsondata.conference}/presentations/`;
        const fetchConfig = {
            method: "post",
            body: json,
            headers: { 'Content-Type': 'application/json'},
        }
        const response = await fetch(presentationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newPresentation = await response.json();
        }
    })
});
