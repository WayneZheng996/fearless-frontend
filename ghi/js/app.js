window.addEventListener('DOMContentLoaded', async () => {
    function createPlaceHolder(){
        return `
        <div class="card" aria-hidden="true">
        <svg class="bd-placeholder-img card-img-top" width="100%" height="180" focusable="false"><rect width="100%" height="100%" fill="#868e96">
            <div class="card-body">
                <h5 class="card-title placeholder-glow">
                <span class="placeholder col-6"></span>
                </h5>
                <p class="card-text placeholder-glow">
                <span class="placeholder col-7"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-4"></span>
                <span class="placeholder col-6"></span>
                <span class="placeholder col-8"></span>
                </p>
                <a href="#" tabindex="-1" class="btn btn-primary disabled placeholder col-6"></a>
            </div>
        </div>
        `.repeat(6)
    }
    const container = document.querySelector('.container')
    const placeHolderHtml = `
    <h2>Upcoming conferences</h2>
    <div class="row row-cols-3">
    ${createPlaceHolder()}
    </div>
    `
    container.innerHTML = placeHolderHtml;

    function createCard(name, description, pictureUrl, startDate, endDate, location) {
        return `
        <div class="col">
          <div class="card shadow p-0 mb-5 bg-body rounded">
            <img src="${pictureUrl}" class="card-img-top">
            <div class="card-body">
              <h5 class="card-title">${name}</h5>
              <h6 class="card-subtitle mb-2 text-muted">${location}</h6>
              <p class="card-text">${description}</p>
            </div>
            <div class="card-footer">
                ${startDate.getUTCMonth()+1}/${startDate.getUTCDate()}/${startDate.getFullYear()} - ${endDate.getUTCMonth()+1}/${endDate.getUTCDate()}/${endDate.getFullYear()}
            </div>
          </div>
        </div>
        `;
      }

      //<div class="card shadow p-3 mb-5 bg-body rounded">
    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);

      if (!response.ok) {
        throw "Invalid Response received"
    } else {
        const data = await response.json();

        const conferenceCardList = []  // to create a list contain all the conference cards
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();
            const title = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;
            const startDate = new Date(details.conference.starts);
            const endDate = new Date(details.conference.ends);
            const location = details.conference.location.name;
            conferenceCardList.push(createCard(title, description, pictureUrl, startDate, endDate, location));
          }
        }
        let numberOfRows;
        if (conferenceCardList.length%3 !== 0){
            numberOfRows = (conferenceCardList.length - (conferenceCardList.length%3))/3;
            numberOfRows +=1
        }else{
            numberOfRows = conferenceCardList.length/3;
        }
        // splitting out the rows with increment of 3, meaning there should be 3 cols in one row
        // let rowCollectionHtml = ``
        // for (let i=0; i<numberOfRows; i++){
        //     rowCollectionHtml += `
        //     <div class="row align-items-start">
        //     ${conferenceCardList.slice(0, 3).join('')}
        //     </div>
        //     `
        //     conferenceCardList.splice(0,3)
        // }
        let rowCollectionHtml = `
            <h2>Upcoming conferences</h2>
            <div class="row row-cols-3">
                ${conferenceCardList.join('')}
            </div>
        `

        container.innerHTML = rowCollectionHtml;
      }
    } catch (e) {
      // Figure out what to do if an error is raised
      console.log(`Error was received during conference fetching stage: ${e}`)
    }

  });
