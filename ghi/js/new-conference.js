window.addEventListener('DOMContentLoaded', async () => {
    const locationRequestUrl = 'http://localhost:8000/api/locations/';
    const locationRequestResponse = await fetch(locationRequestUrl);
    if (!locationRequestResponse.ok){
        throw "Invalid Request was made fetching Locations list";
    }else{
        const locationList = await locationRequestResponse.json()
        const locationListFormatted = []

        for (let i=0; i< locationList['locations'].length; i++){
            let locationListSplit = locationList['locations'][i].href.split('/')
            let locationId = locationListSplit[locationListSplit.length-2]
            locationListFormatted.push(`
            <option value="${parseInt(locationId)}">${locationList['locations'][i].name}</option>
            `)
        }
        const locationListHtml = `
        <option selected value="">Choose a Location</option>
        ${locationListFormatted.join('')}
        `
        const LocationTag = document.querySelector('.form-select')
        LocationTag.innerHTML = locationListHtml
    }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event=> {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: { 'Content-Type': 'application/json'},
        }
        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newConference = await response.json();
            console.log(newConference)
        }
    })
});
