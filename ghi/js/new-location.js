window.addEventListener('DOMContentLoaded', async () => {
    const stateRequestUrl = 'http://localhost:8000/api/states/';
    const stateRequestResponse = await fetch(stateRequestUrl);
    if (!stateRequestResponse.ok){
        throw "Invalid Request was made fetching Statue list";
    }else{
        const stateList = await stateRequestResponse.json()
        const stateListFormatted = []
        for (let i=0; i< stateList['states'].length; i++){
            stateListFormatted.push(`
            <option value="${stateList['states'][i].abbreviation}">${stateList['states'][i].name}</option>
            `)
        }
        const stateListHtml = `
        <option selected value="">Choose a state</option>
        ${stateListFormatted.join('')}
        `
        const stateTag = document.querySelector('.form-select')
        stateTag.innerHTML = stateListHtml
    }

    const formTag = document.getElementById('create-location-form');
    formTag.addEventListener('submit', async event=> {
        event.preventDefault();
        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));
        const locationUrl = 'http://localhost:8000/api/locations/';
        const fetchConfig = {
            method: "post",
            body: json,
            headers: { 'Content-Type': 'application/json'},
        }
        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
            formTag.reset();
            const newLocation = await response.json();
        }

    // Method 1:
    //   formTag.forEach(function(key,value){
    //     formData[key] = value
    //   });
    // Method 2:
      // could also do it like formTag.forEach((key,value) => formData[key]=value);
    });

  });
// window.addEventListener('submit', (event) => {});
// onsubmit = (event) => { };
