window.addEventListener('DOMContentLoaded', async () => {
    const Option = {
        name: 'string',
        url: document.URL
      }

    const payloadCookie = await cookieStore.get('jwt_access_payload')
    console.log(payloadCookie)
    if (payloadCookie) {
        // The cookie value is a JSON-formatted string, so parse it
        // const encodedPayload = JSON.parse(payloadCookie.value);

        // Convert the encoded payload from base64 to normal string
        const decodedPayload = atob(payloadCookie.value)

        // The payload is a JSON-formatted string, so parse it
        const payload = JSON.parse(decodedPayload)

        // Print the payload
        // console.log(payload);
        // console.log(Object.values(payload['user']['perms']))

        // Check if "events.add_conference" is in the permissions.
        // If it is, remove 'd-none' from the link
        if (Object.values(payload['user']['perms']).indexOf('events.add_conference') > -1){
            const conferenceTag = document.getElementById('nav-newConference')
            conferenceTag.className = 'nav-link'
        }


        // Check if "events.add_location" is in the permissions.
        // If it is, remove 'd-none' from the link
        if (Object.values(payload['user']['perms']).indexOf('events.add_location') > -1){
            const locationTag = document.getElementById('nav-newLocation')
            locationTag.className = 'nav-link'
        }

    }
  });
